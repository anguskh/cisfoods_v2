/* JS is for sticky header  */
$(window).scroll(function(){
  if ($(window).scrollTop() > 160) {
    $('.header-bottom-block').addClass('fixed');
   }
   else {
    $('.header-bottom-block').removeClass('fixed');
   }
});

/* JS is for left category tree view  */
function categorytreeview(){

    if($('.left-category').hasClass('treeview')==true){
        $(".treeview-list").treeview({
        animated: "slow",
        collapsed: true,
        unique: true
    });
    $('.left-category.treeview-list a.active').parent().removeClass('expandable');
    $('.left-category.treeview-list a.active').parent().addClass('collapsable');
    $('.left-category.treeview-list .collapsable > ul.collapsable').css('display','block');
    }
}
$(document).ready(function(){categorytreeview();});

/*----------- menu hover -------------------*/

$(document).ready(function () {
  var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
);
wow.init();
    $('#tabs a').tabs();
    $('#responsive-menu').appendTo('.header-bottom-left');
    $('.prodbottominfo').appendTo('#product');
    $('.category_thumb2').appendTo('.content-top-breadcum');
     //*-----------------------Parallax------------------------ *//

    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    if(!isMobile) {
        if($(".cms_parallax,.cms_parallax2").length){  $(".cms_parallax,.cms_parallax2").sitManParallex({  invert: false });};
    }else{
        $(".cms_parallax,.cms_parallax2").sitManParallex({  invert: true });
    }
});


/* JS for Top button */
$(document).ready(function(){
$("body").append("<a id='scrollup' title='Back To Top' href=''></a>");
$('#content h1').prependTo('ul.breadcrumb');

//$('ul.breadcrumb').prependTo('#content');
(function($) {
    $(document).ready(function(){

        //When distance from top = 250px fade button in/out
        $(window).scroll(function(){
                if ($(window).scrollTop() > 200) {
                    $('#scrollup').fadeIn(300);
                 }
                  else {
                    $('#scrollup').fadeOut(300);
                 }
        });
        //On click scroll to top of page t = 1000ms
        $('#scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 1000);
            return false;
        });
    });
})(jQuery);
});


/* JS is for Menu more link */
$(document).ready(function () {
            if ($(window).width() > 992 && $(window).width() < 1200){
                var max_elem = 6 ;
                $('.main-navigation > li').first().addClass('home_first');
                var items = $('.main-navigation > li.main_cat');
                var surplus = items.slice(max_elem, items.length);
                surplus.wrapAll('<li class="level-top hiden_menu menu-last"><ul>');
                $('.hiden_menu').prepend('<a href="#" class="level-top">More</a>');
            }
            if ($(window).width() > 1199){
                var max_elem = 10;
                $('.main-navigation > li').first().addClass('home_first');
                var items = $('.main-navigation > li.main_cat');
                var surplus = items.slice(max_elem, items.length);
                surplus.wrapAll('<li class="level-top hiden_menu menu-last"><ul>');
                $('.hiden_menu').prepend('<a href="#" class="level-top">More</a>');
            }

            $('.qtyplus').click(function () {
                $(this).prev().val(+$(this).prev().val() + 1); 
            });
            $('.qtyminus').click(function () {
                if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
                }
            });

});



/* JS is for cart and filter position */
function cartrespo(){
if ($(window).width() <= 767){
    $('#cart').appendTo('.cart_icon');
    $('#ajaxfilter').appendTo('.filter-product');
    $('#filter-view').appendTo('.compare-total');
    $('.header-search').appendTo('.search_icon');
}
else{
    $('#cart').appendTo('.header-cart');
    $('#ajaxfilter').appendTo('#column-left');
    $('.header-search').appendTo('.header-top-right');
}
}
$(window).ready(function(){cartrespo();});
$(window).resize(function(){cartrespo();});

/* JS is for static menu link position */
function menulink(){
if ($(window).width() < 992){
    $('.header-menu .header_menulink li').appendTo('#responsive-menu ul.nav');
}
else{
    $('.header-menu .header_menulink li').appendTo('ul.main-navigation');
    $('.category_block .header_menulink li').appendTo('ul.left-category');
}
}
$(window).ready(function(){menulink();});
$(window).resize(function(){menulink();});



