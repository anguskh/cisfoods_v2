<?php
class ControllerExtensionModuleAstores extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/astores');
      static $module = 0;	
		$this->load->model('extension/module/astores');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

            $results=$this->model_extension_module_astores->getAstores();

           $data['module_astores_awidth'] = $this->config->get('module_astores_awidth');
			

			$data['module_astores_aheight'] = $this->config->get('module_astores_aheight');

            foreach($results as $result)
            {
               $data['astores'][]=array(
				'name'            => $result['name'],
				'sign'            => $result['sign'],
				'image'     	   => $result['image'],				
				'thumb'     	   => $this->model_tool_image->resize($result['image'],$data['module_astores_awidth'],$data['module_astores_aheight']),				
				'description'      => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
				'status'       => $result['status'],
				'sort_order' => $result['sort_order']
			);
             

            }
                 //echo'<pre>';print_r($data);die;
			$data['module_astores_status'] = $this->config->get('module_astores_status');
		

			$data['module_astores_aheight'] = $this->config->get('module_astores_aheight');
		

			$data['module_astores_awidth'] = $this->config->get('module_astores_awidth');


			$data['module_astores_heading'] = $this->config->get('module_astores_heading');
		

			          

            $data['module'] = $module++;
        
           // $this->response->setOutput($this->load->view('extension/module/astores', $data));
			return $this->load->view('extension/module/astores', $data);
		
	}
}