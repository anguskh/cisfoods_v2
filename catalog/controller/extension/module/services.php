<?php
class ControllerExtensionModuleServices extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/services');
      static $module = 0;	
		$this->load->model('extension/module/services');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

            $results=$this->model_extension_module_services->getServices();

           $data['module_services_awidth'] = $this->config->get('module_services_awidth');
		

			$data['module_services_aheight'] = $this->config->get('module_services_aheight');

            foreach($results as $result)
            {
               $data['services'][]=array(
				'name'            => $result['name'],
				'image'     	   => $result['image'],
				'thumb'     	   => $this->model_tool_image->resize($result['image'],$data['module_services_awidth'],$data['module_services_aheight']),
				'description'      => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
				'status'       => $result['status'],
				'sort_order' => $result['sort_order']
			);
             

            }
                 //echo'<pre>';print_r($data);die;
			$data['module_services_status'] = $this->config->get('module_services_status');
		

			$data['module_services_aheight'] = $this->config->get('module_services_aheight');
		

			$data['module_services_awidth'] = $this->config->get('module_services_awidth');


			$data['module_services_heading'] = $this->config->get('module_services_heading');
		

			$data['module_services_bgclr'] = $this->config->get('module_services_bgclr');
		

			$data['module_services_fontclr'] = $this->config->get('module_services_fontclr');

        
          

            $data['module'] = $module++;
        
           // $this->response->setOutput($this->load->view('extension/module/services', $data));
			return $this->load->view('extension/module/services', $data);
		
	}
}