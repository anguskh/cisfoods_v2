<?php
class ControllerExtensionModuleFeaturedCategory extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featuredcategory');

		$this->load->model('catalog/category');

		$this->load->model('tool/image');

		$data['categories'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['categoriesadded'])) {

            $data['box_title'] = $setting['name'];

            $categories_data = array();

			foreach ($setting['categoriesadded'] as $category_id) {
				$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
                    $categories_data[] = $category_info;
				}
			}

			$categories = array_slice($categories_data, 0, (int)$setting['limit']);

			foreach ($categories as $category) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}
				if ($category['image_2']) {
					$image = $this->model_tool_image->resize($category['image_2'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				$data['categories'][] = array(
					'category_id'  => $category['category_id'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'thumb2'       => $image,
					'name'        => $category['name'],
					'href'        => $this->url->link('product/category', 'language=' . $this->config->get('config_language') . '&path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->config->get('config_language');

		if ($data['categories']) {
			return $this->load->view('extension/module/featuredcategory', $data);
		}
	}
}