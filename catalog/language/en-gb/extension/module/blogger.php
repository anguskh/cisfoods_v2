<?php
// Heading 
$_['heading_title']    = 'Latest News';
$_['sub_heading_title'] = 'From the blog';
// Text
$_['text_read_more']   = 'read more';
$_['text_date_added']  = 'Date Added:';
$_['entry_comment']       = 'Comments';

// Button
$_['button_all_blogs'] = 'See all Blogs';