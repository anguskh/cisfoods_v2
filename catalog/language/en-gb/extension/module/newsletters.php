<?php
// Heading
$_['newsletter_title']    = 'Subscribe for Best offer';
$_['sub_heading_title']    = 'Newsletter';
$_['email']='Email';
$_['subscribe']='Subscribe';
$_['text_success_add']     = 'Subscription Success';
$_['text_success_already']   = 'Email Is already Subscribed';
$_['text_subscribe_placeholder']   = 'Enter your email address';
$_['newsletter_static_title1']   = 'Hungry?';
$_['newsletter_static_title2']   = '';
$_['newsletter_static_desc1']   = 'Get 50% Off on all dishes*';
$_['newsletter_static_desc2']   = 'Use Code: <span>DIS50</span>';

/* Popup Text */


$_['chk_text']       = 'Do not show again';

