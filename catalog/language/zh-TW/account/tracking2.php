<?php
// Heading
$_['heading_title']    = '推薦追蹤';

// Text
$_['text_account']     = '我的帳號';
$_['text_description'] = '為了確保您可以獲得推薦獎金，我們需要在URL加上我們的追踪碼，您可以使用下面的工具來產生％s網站的追蹤連結。';
$_['text_unapproved']  = '很抱歉，您的推薦帳號尚未獲得核准，您須等待推薦帳號通過核准之後，才能使用此項功能。';

// Entry
$_['entry_code']       = '您的推薦追蹤碼';
$_['entry_generator']  = '選擇推薦商品';
$_['entry_link']       = '商品頁追蹤連結';
$_['entry_homelink']   = '首頁追蹤連結';
$_['entry_category']   = '選擇推薦分類';
$_['entry_catlink']    = '分類頁追蹤連結';

// Help
$_['help_generator']  = '輸入您要產生追蹤連結的商品';