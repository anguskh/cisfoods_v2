<?php
// Heading
$_['heading_title']                     = '台灣萬事達物流';

// Text

$_['text_b2c']                       = '大宗寄件';
$_['text_c2c']                       = '超商寄件';


$_['text_choice']                       = '選擇收件門市';
$_['text_rechoice']                     = '重新選擇收件門市';
$_['text_store_name']                   = '門市名稱：';
$_['text_store_address']                = '門市地址：';
$_['text_store_tel']                    = '門市電話：';
$_['text_store_info']                   = '收件門市';

// Error
$_['error_no_storeinfo']                = '尚未選擇收件門市';