<?php
// Heading 
$_['heading_title']         = '推薦報表';

// Text
$_['text_account']          = '我的帳號';
$_['text_order']            = '訂單資訊';
$_['text_order_detail']     = '訂單明細';
$_['text_invoice_no']       = '發票號碼';
$_['text_order_id']         = '訂單編號';
$_['text_date_added']       = '訂單日期';
$_['text_shipping_address'] = '運送地址';
$_['text_shipping_method']  = '運送方式';
$_['text_payment_address']  = '帳單地址';
$_['text_payment_method']   = '付款方式';
$_['text_comment']          = '訂單備註';
$_['text_history']          = '訂單記錄';
$_['text_success']          = '成功：您已經新增商品<a href="%s">%s</a>到您的<a href="%s">購物車</a>！';
$_['text_empty']            = '目前還沒有任何訂單記錄！';
$_['text_error']            = '找不到您查詢的訂單！';
$_['text_unapproved']  		= '很抱歉，您的推薦帳號尚未獲得核准，您須等待推薦帳號通過核准之後，才能使用此項功能。';

// Column
$_['column_order_id']       = '訂單編號';
$_['column_customer']       = '會員';
$_['column_quantity']       = '數量';
$_['column_price']          = '售價';
$_['column_total']          = '訂單金額';
$_['column_commission']     = '推薦佣金';
$_['column_action']         = '操作';
$_['column_date_added']     = '訂單日期';
$_['column_status']         = '訂單狀態';

$_['column_total_visitors'] = '訪客數';
$_['column_total_orders']   = '有效訂單數';
$_['column_total_amount']   = '消費金額';
$_['column_commission_total']   = '分潤金額';

// Error
$_['error_reorder']         = '%s 目前無法被回購。';