<?php
// Heading
$_['heading_title']    = '防止詐騙模組';

// Text
$_['text_success']     = '成功: 防止詐騙模組設定已更新!';
$_['text_list']        = '防止詐騙清單';

// Column
$_['column_name']      = '防止詐騙模組名稱';
$_['column_status']    = '狀態';
$_['column_action']    = '管理';

// Error
$_['error_permission'] = '警告: 您沒有權限修改防止詐騙模組!';