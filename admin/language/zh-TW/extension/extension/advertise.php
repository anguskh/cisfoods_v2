<?php

// Heading
$_['heading_title'] = "廣告管理";


// Columns
$_['column_name']   = "廣告名稱";
$_['column_status'] = "狀態";
$_['column_action'] = "操作";


// Text
$_['text_success']   = "<strong>成功:</strong> 廣告管理設定已更新!";

// Error
$_['error_adblock'] = "It looks like you are using an ad blocker. In order to use this Advertising section, please disable your ad blocker for your OpenCart admin panel.";
