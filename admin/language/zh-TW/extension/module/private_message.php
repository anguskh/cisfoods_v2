<?php
// Heading
$_['heading_title']    = '站內訊息';

// Text
$_['text_extension']   = '擴充功能';
$_['text_success']     = '成功: 站內訊息模組設定已更新！';
$_['text_edit']        = '編輯站內訊息模組';

// Entry
$_['entry_status']     = '狀態';

// Error
$_['error_permission'] = '警告: 您沒有權限修改站內訊息模組！';