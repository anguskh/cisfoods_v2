<?php
$_['heading_title'] 			= '推薦機制擴充模組';

$_['error_permission'] 			= '警告: 您沒有權限編輯推薦機制擴充模組!';
$_['error_affiliate_term1'] 	= '警告: 佣金支付方式說明必須輸入!';
$_['error_affiliate_term2'] 	= '警告: 佣金轉帳手續費說明必須輸入!';


$_['entry_status'] 				= '狀態';
$_['entry_language'] 			= '語系文字';
$_['entry_affiliate_term1'] 	= '佣金支付方式說明';
$_['entry_affiliate_term2'] 	= '佣金轉帳手續費說明';

$_['text_extension'] 			= '擴充模組';
$_['text_success'] 				= '成功: 推薦機制擴充模組已更新設定!';
$_['text_edit'] 				= '編輯推薦機制擴充模組';

