<?php
$_['heading_title'] 	= '自訂訂單編號模組';

$_['error_permission'] 	= '警告: 您沒有權限編輯自訂訂單編號模組!';


$_['entry_status'] 		= '狀態';

$_['text_extension'] 	= '擴充模組';
$_['text_success'] 		= '成功: 自訂訂單編號模組已更新設定!';
$_['text_edit'] 		= '編輯自訂訂單編號模組';

