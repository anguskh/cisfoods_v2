<?php
// Heading
$_['heading_title']   = '找回您的密碼？';

// Text
$_['text_forgotten']  = '忘記密碼';
$_['text_your_email'] = '您的 Email';
$_['text_email']      = '輸入您的 Email，密碼將透過 Email 發送給您。';
$_['text_success']    = '確認信件已發送至您的信箱。';

// Entry
$_['entry_email']     = 'E-Mail:';
$_['entry_password']  = '新密碼:';
$_['entry_confirm']   = '確認密碼:';

// Error
$_['error_email']     = '警告: 沒有找到對應的 Email，請再試一次！';
$_['error_password']  = '密碼必須在 4 至 20 個字之間！';
$_['error_confirm']   = '密碼和確認密碼不相符！';