<?php
// Heading
$_['heading_title']     		= '推薦訂單分潤報表';

// Text
$_['text_extension']    		= '擴充模組';
$_['text_list']         		= '推薦訂單分潤報表';
$_['text_filter']       		= '依月份篩選';
$_['text_guest']        		= '訪客';

// Column
$_['column_order_id']			= '訂單編號';
$_['column_customer']			= '會員名稱';
$_['column_status']				= '狀態';
$_['column_date_added']			= '產生日期';
$_['column_total']				= '總計';
$_['column_commission']			= '分潤%數';
$_['column_commission_amount']	= '佣金';


// Entry
$_['entry_month']				= '報表月份';
$_['entry_sort_order']  		= '排序';

// Error
$_['error_permission']  		= '警告：您沒有權限編輯推薦訂單分潤報表!';