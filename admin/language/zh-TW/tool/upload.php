<?php
// Heading
$_['heading_title']     = '上傳檔案';

// Text
$_['text_success']      = '成功: 上傳檔案設定已更新！';
$_['text_list']         = '上傳檔案清單';

// Column
$_['column_name']       = '上傳檔案名稱';
$_['column_filename']   = '檔案名稱';
$_['column_date_added'] = '新增日期';
$_['column_action']     = '管理';

// Entry
$_['entry_name']        = '上傳檔案名稱';
$_['entry_filename']    = '檔案名稱';
$_['entry_date_added'] 	= '新增日期';

// Error
$_['error_permission']  = '警告: 您沒有權限修改上傳檔案！';
$_['error_upload']      = '無效的上傳檔案!';
$_['error_file']        = '找不到上傳檔案!';