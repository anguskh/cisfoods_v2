<?php
// Heading
$_['heading_title']			= '公告訊息管理';

// Text
$_['text_success']			= '成功: 公告訊息設定已更新！';
$_['text_list']				= '公告訊息清單';
$_['text_add']				= '新增公告訊息';
$_['text_edit']				= '編輯公告訊息';
$_['text_default']	 		= '預設';
$_['text_keyword']          = '請勿使用空格，使用 - 取代空格，並確定靜態網址(SEO URL)是沒有重複過的。';

// Column
$_['column_title']			= '公告訊息標題';
$_['column_date_start']		= '上線時間';
$_['column_date_end']		= '下線時間';
$_['column_date_added']		= '編輯時間';
$_['column_customer_group']	= '通知會員群組';
$_['column_customer_name']	= '通知對象';
$_['column_sort_order']		= '排序';
$_['column_action']			= '管理';

// Entry
$_['entry_title']			= '公告訊息標題';
$_['entry_content']			= '公告訊息內容';
$_['entry_date_start']		= '上線時間';
$_['entry_date_end']		= '下線時間';
$_['entry_customer_group']	= '會員群組';
$_['entry_customer']		= '會員';
$_['entry_sort_order']		= '公告訊息排序';
$_['entry_status']			= '狀態';

// Error 
$_['error_warning']			= '警告: 資料未正確輸入！';
$_['error_permission']		= '警告: 您沒有權限編輯公告訊息！';
$_['error_title']			= '公告訊息標題必須是 1 到 64 個字！';
$_['error_description']		= '公告訊息內容長度不得少於 3 個字！';
$_['error_meta_title']		= 'Meta 標題必須是 1 到 255 個字！';
$_['error_seo']				= '靜態網址(SEO URL)必須輸入!';
$_['error_keyword']			= '靜態網址(SEO URL)必須是沒有重複的!';
