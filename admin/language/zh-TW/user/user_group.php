<?php
// Heading
$_['heading_title']    = '管理員群組';

// Text
$_['text_success']     = '成功: 管理員群組設定已更新！';
$_['text_list']        = '管理員群組';
$_['text_add']         = '新增管理員群組';
$_['text_edit']        = '編輯管理員群組';

// Column
$_['column_name']      = '管理員群組名稱';
$_['column_action']    = '操作';

// Entry
$_['entry_name']       = '管理員群組名稱';
$_['entry_access']     = '檢視權限';
$_['entry_modify']     = '修改權限';

// Error
$_['error_permission'] = '警告: 您沒有權限更改管理員群組！';
$_['error_name']       = '管理員群組名稱長度必須是 3 到 64 個字！';
$_['error_user']       = '警告: 此管理員帳號群組不能被刪除，因為已有 %s 位管理員帳號屬於此群組！';