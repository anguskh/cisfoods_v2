<?php
// Heading
$_['heading_title']    = 'About Stores';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Stores module!';
$_['text_edit']        = 'Edit Stores Module';

// Entry
$_['entry_head']     = 'Widget Heading';
$_['entry_image']     = 'Widget background image';

$_['entry_font_clr']     = 'Widget Font color';
$_['entry_bgclr']     = 'Widget background color';
$_['entry_status']     = 'Status';
$_['entry_author_size']     = 'Logo image size';

// Errorentry_head
$_['error_permission'] = 'Warning: You do not have permission to modify Stores module!';