<?php
// Heading
$_['heading_title']    = 'FAQs';
$_['breadcrumb_title']    = 'FAQs';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified FAQ module!';
$_['text_edit']        = 'Edit FAQ Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_width']		='Width';
$_['entry_height']		='Height';
$_['entry_limit']		='Set Faqs Limit';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify FAQ module!';