<?php
// Heading
$_['heading_title']    = 'Services';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Services module!';
$_['text_edit']        = 'Edit Services Module';

// Entry
$_['entry_head']     = 'Widget Heading';
$_['entry_image']     = 'Widget background image';

$_['entry_font_clr']     = 'Widget Font color';
$_['entry_bgclr']     = 'Widget background color';
$_['entry_status']     = 'Status';
$_['entry_author_size']     = 'Icon image size';

// Errorentry_head
$_['error_permission'] = 'Warning: You do not have permission to modify Services module!';