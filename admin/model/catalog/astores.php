<?php
class ModelCatalogAstores extends Model {
	public function addAstore($data) { 
   
		$this->db->query("INSERT INTO " . DB_PREFIX . "astores_description SET  status = '" .$data['status'] . "', sort_order='".$data['sort_order']."' ");
      
      $astore_id = $this->db->getLastId();


		foreach ($data['astores'] as $language_id => $value) {			
			$this->db->query("INSERT INTO " . DB_PREFIX . "astores SET astore_id = '" . (int)$astore_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', sign = '" . $this->db->escape($value['sign']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "'");
        
		}

		//die;
  }

  public function getTotalAstores() { 
  // die;
       $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "astores_description");

		return $query->row['total'];
  }

  public function getAstores($data = array()) {

		if ($data) {
			$sql ="SELECT * FROM " . DB_PREFIX . "astores t LEFT JOIN " . DB_PREFIX . "astores_description td ON (t.astore_id = td.astore_id) WHERE t.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				't.name',
				'td.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY t.name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;

		} else {
			$astores_data = $this->cache->get('astores.' . (int)$this->config->get('config_language_id'));

			if (!$astores_data) {

				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "astores  WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY title");

				$astores_data = $query->rows;

				$this->cache->set('astores.' . (int)$this->config->get('config_language_id'), $astores_data);
			}

			return $astores_data;
		}
	}

      public function getAstore($astore_id) {
      	 //echo $astore_id;die('sss');
      //	echo "SELECT DISTINCT * FROM " . DB_PREFIX . "testimonals WHERE astore_id = '" . (int)$astore_id . "'";die;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "astores WHERE astore_id = '" . (int)$astore_id . "'");
    
		return $query->row;
	}


    public function getAstoreDescriptions($astore_id) {
    //die('db');
    $astore_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "astores_description WHERE astore_id = '" . (int)$astore_id . "'");

		return $query->rows;
    }
     public function getAstoress($astore_id) {
    //die('db');
    $astore_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "astores WHERE astore_id = '" . (int)$astore_id . "'");

		return $query->rows;
    }


    public function editAstores($astore_id,$data) { 
     // echo '<pre>'; print_r($data);die;
		$this->db->query("UPDATE " . DB_PREFIX . "astores_description SET  status = '" .$data['status'] . "',  sort_order='".$data['sort_order']."' WHERE astore_id='".$astore_id."' ");
      
       $this->db->query("DELETE FROM " . DB_PREFIX . "astores WHERE astore_id='".$astore_id."'");


		foreach ($data['astores'] as $language_id => $value) {
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "astores SET astore_id = '" . (int)$astore_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', sign = '" . $this->db->escape($value['sign']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "' ");
        
		}
 
    }	


     public function deleteAstore($astore_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "astores` WHERE astore_id = '" . (int)$astore_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "astores_description` WHERE astore_id = '" . (int)$astore_id . "'");
	

		$this->cache->delete('astores');
	}


	
}
