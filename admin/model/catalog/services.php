<?php
class ModelCatalogServices extends Model {
	public function addService($data) { 
   //echo "INSERT INTO " . DB_PREFIX . "services SET sort_order = '" .$data['sort_order'] . "',  status = '" .$data['status'] . "', name='".$data['name']."', image='".$data['image']."', description='".$data['description']."', name='".$data['image']."', status='".$data['status']."', sort_order='".$data['sort_order']."' "; die;
	//die('yes');
		//echo'<pre>';print_r($data);die;
		$this->db->query("INSERT INTO " . DB_PREFIX . "services_description SET  status = '" .$data['status'] . "', sort_order='".$data['sort_order']."' ");
      
      $service_id = $this->db->getLastId();


		foreach ($data['services'] as $language_id => $value) {
			//echo "INSERT INTO " . DB_PREFIX . "services SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "'";die;
			//echo '<pre>';print_r($value);
			//echo (int)$language_id.(int)$service_id;
			//echo "INSERT INTO " . DB_PREFIX . "services SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "'";die;
			$this->db->query("INSERT INTO " . DB_PREFIX . "services SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "'");
        
		}

		//die;
  }

  public function getTotalServices() { 
  // die;
       $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "services_description");

		return $query->row['total'];
  }

  public function getServices($data = array()) {

		if ($data) {
			$sql ="SELECT * FROM " . DB_PREFIX . "services t LEFT JOIN " . DB_PREFIX . "services_description td ON (t.service_id = td.service_id) WHERE t.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				't.name',
				'td.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY t.name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;

		} else {
			$services_data = $this->cache->get('services.' . (int)$this->config->get('config_language_id'));

			if (!$services_data) {

				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services  WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY title");

				$services_data = $query->rows;

				$this->cache->set('services.' . (int)$this->config->get('config_language_id'), $services_data);
			}

			return $services_data;
		}
	}

      public function getService($service_id) {
      	 //echo $service_id;die('sss');
      //	echo "SELECT DISTINCT * FROM " . DB_PREFIX . "testimonals WHERE service_id = '" . (int)$service_id . "'";die;
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "services WHERE service_id = '" . (int)$service_id . "'");
    
		return $query->row;
	}


    public function getServiceDescriptions($service_id) {
    //die('db');
    $service_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services_description WHERE service_id = '" . (int)$service_id . "'");

		return $query->rows;
    }
     public function getServicess($service_id) {
    //die('db');
    $service_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services WHERE service_id = '" . (int)$service_id . "'");

		return $query->rows;
    }


    public function editServices($service_id,$data) { 
     // echo '<pre>'; print_r($data);die;
		$this->db->query("UPDATE " . DB_PREFIX . "services_description SET  status = '" .$data['status'] . "',  sort_order='".$data['sort_order']."' WHERE service_id='".$service_id."' ");
      
       $this->db->query("DELETE FROM " . DB_PREFIX . "services WHERE service_id='".$service_id."'");


		foreach ($data['services'] as $language_id => $value) {
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "services SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['title']) . "', image = '" . $this->db->escape($value['image']) . "', description = '" . $this->db->escape($value['description']) . "' ");
        
		}
 
    }	


     public function deleteService($service_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "services` WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "services_description` WHERE service_id = '" . (int)$service_id . "'");
	

		$this->cache->delete('services');
	}


	
}
