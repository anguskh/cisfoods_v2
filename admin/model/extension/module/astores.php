<?php

class ModelExtensionModuleAstores extends Model {

	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "astores` (
           `id` int(200) NOT NULL AUTO_INCREMENT,
           `astore_id` int(200) NOT NULL,
           `language_id` varchar(200) NOT NULL,
           `store_id` varchar(200) DEFAULT NULL,
           `name` varchar(100) NOT NULL,
           `sign` varchar(100) DEFAULT NULL,
           `description` text NOT NULL,
           `image` varchar(100) NOT NULL,
           PRIMARY KEY (`id`)
           ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

		$this->db->query("
   			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "astores_description` (
               `astore_id` int(200) NOT NULL AUTO_INCREMENT,
               `status` int(100) NOT NULL,
               `sort_order` int(100) NOT NULL,
                PRIMARY KEY (`astore_id`)
              ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");


    
       }

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "astores`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "astores_description`;");
		
	}
}

