<?php

class ModelExtensionModuleServices extends Model {

	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "services` (
           `id` int(200) NOT NULL AUTO_INCREMENT,
           `service_id` int(200) NOT NULL,
           `language_id` varchar(200) NOT NULL,
           `store_id` varchar(200) DEFAULT NULL,
           `name` varchar(100) NOT NULL,
           `description` text NOT NULL,
           `image` varchar(100) NOT NULL,
           PRIMARY KEY (`id`)
           ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

		$this->db->query("
   			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "services_description` (
               `service_id` int(200) NOT NULL AUTO_INCREMENT,
               `status` int(100) NOT NULL,
               `sort_order` int(100) NOT NULL,
                PRIMARY KEY (`service_id`)
              ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");


    
       }

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "services`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "services_description`;");
		
	}
}

