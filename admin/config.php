<?php
define('SERVER_ADDRESS', $_SERVER['HTTP_HOST']) ;
// HTTP
define('HTTP_SERVER',	'http://'.SERVER_ADDRESS.'/admin/');
define('HTTP_CATALOG',	'http://'.SERVER_ADDRESS.'/');

// HTTPS
define('HTTPS_SERVER',	'http://'.SERVER_ADDRESS.'/admin/');
define('HTTPS_CATALOG',	'http://'.SERVER_ADDRESS.'/');

// DIR
define('DIR_INITIAL',		'/www/wwwroot/ccs') ;
define('DIR_APPLICATION',	DIR_INITIAL . '/admin/');
define('DIR_SYSTEM',		DIR_INITIAL . '/system/');
define('DIR_IMAGE',			DIR_INITIAL . '/image/');
define('DIR_STORAGE',		DIR_SYSTEM  . 'storage/');
define('DIR_CATALOG',		DIR_INITIAL . '/catalog/');
define('DIR_LANGUAGE',		DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE',		DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG',		DIR_SYSTEM  . 'config/');
define('DIR_CACHE',			DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD',		DIR_STORAGE . 'download/');
define('DIR_LOGS',			DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION',	DIR_STORAGE . 'modification/');
define('DIR_SESSION',		DIR_STORAGE . 'session/');
define('DIR_UPLOAD',		DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'ccs');
define('DB_PASSWORD', 'mamLiGDCSLir67PZ');
define('DB_DATABASE', 'ccs');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
